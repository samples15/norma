module.exports = {
  // Register all css helpers globally
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/_scoped.scss";
        `,
      },
    },
  },
};
