// Vue
import Vue from 'vue';

// App
import App from './App';

// Router
import router from './router';

// Store
import store from './store';

// Template styles
import '@/styles/_template.scss';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
