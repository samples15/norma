import axios from 'axios';

//-------------------------------
// Stations
//-------------------------------

export const stationsRequest = () => new Promise((resolve, reject) => {
  const api = 'https://toronto-us.publicbikesystem.net/ube/gbfs/v1/en/station_information';
  axios.get(api)
    .then((res) => {
      resolve(res.data.data.stations);
    })
    .catch((err) => {
      reject(err);
    });
});

//-------------------------------
// Bikes
//-------------------------------

export const bikesRequest = () => new Promise((resolve, reject) => {
  const api = 'https://toronto-us.publicbikesystem.net/ube/gbfs/v1/en/station_status';
  axios.get(api)
    .then((res) => {
      resolve(res.data.data.stations);
    })
    .catch((err) => {
      reject(err);
    });
});
