// Vue
import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import dataStore from './data';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    data: dataStore,
  },
});

export default store;
