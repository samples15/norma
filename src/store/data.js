// Services/Api
import {
  stationsRequest,
  bikesRequest,
} from '@/services/api';

// Export
export default {
  namespaced: true,

  // State
  state() {
    return {
      stations: localStorage.getItem('stations') ? JSON.parse(localStorage.getItem('stations')) : [],
      bikes: localStorage.getItem('bikes') ? JSON.parse(localStorage.getItem('bikes')) : [],
      //
      search: localStorage.getItem('search') ? localStorage.getItem('search') : '',
    };
  },

  // Getters
  getters: {
    stationsList: (state) => state.stations,
    bikesList: (state) => state.bikes,
    //
    flexSearch: (state) => state.search,
  },

  // Actions
  actions: {
    requestStations({ commit }) {
      return new Promise((resolve, reject) => {
        stationsRequest()
          .then((res) => {
            commit('setStations', res);
            localStorage.setItem('stations', JSON.stringify(res));
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },

    requestBikes({ commit }) {
      return new Promise((resolve, reject) => {
        bikesRequest()
          .then((res) => {
            commit('setBikes', res);
            localStorage.setItem('bikes', JSON.stringify(res));
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },

    //
    requestSaveSearch({ commit }, str) {
      commit('setUserSearch', str);
      localStorage.setItem('search', str);
    },
  },

  // Mutations
  mutations: {
    setStations(state, payload) {
      state.stations = payload;
    },
    setBikes(state, payload) {
      state.bikes = payload;
    },
    //
    setUserSearch(state, payload) {
      state.search = payload;
    },
  },
};
